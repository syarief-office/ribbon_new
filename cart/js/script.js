

jQuery(document).ready(function(){

 	//TOOLTIP
	jQuery( "#show-tooltip").tooltip({
	  position: {
	    my: "center bottom-20",
	    at: "center top",
	    using: function( position, feedback ) {
	      jQuery( this ).css( position );
	      jQuery( "<div>" )
	        .addClass( "arrow" )
	        .addClass( feedback.vertical )
	        .addClass( feedback.horizontal )
	        .appendTo( this );
	    }
	  },
	   show: {
	    effect: "slideDown",
	    delay: 180
	  }
	});

	//SELECT STYLE
	jQuery('.selectpicker').selectpicker();

	//NAV TO SELECT
	jQuery("<select />").appendTo("#cart-nav");
		// Create default option "Go to..."
		jQuery("<option />", {
		   "selected": "selected",
		   "value"   : "",
		   "text"    : "Go to..."
		}).appendTo("#cart-nav select");

		// Populate dropdown with menu items
		jQuery("#cart-nav a").each(function() {
		 var el = jQuery(this);
		 jQuery("<option />", {
		     "value"   : el.attr("href"),
		     "text"    : el.text()
		 }).appendTo("#cart-nav select");
	});

});