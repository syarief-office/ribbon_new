/* Author:http://www.rainatspace.com

*/

jQuery(document).ready(function(){
	//RESPONSIVE NAV
	jQuery('ul#menu').slicknav({
		label: '',
		duration: 1000
	});

	//EQUAL HEIGHT
	function equalHeight(group) {
		tallest = 0;
		jQuery(window).on("load resize", function(){
			if (jQuery(window).width() > 979 ) {
				group.each(function() {
					thisHeight = jQuery(this).height();
					if(thisHeight > tallest) {
						tallest = thisHeight;
					}
				});
				group.height(tallest);
			}
		});
	}
	equalHeight(jQuery(".eqHeight"));

	jQuery("#hide").on('click', function(){
		jQuery('.top-notif').fadeOut('slow');

	});

	//LAZYLOAD
	jQuery("img.lazy").lazyload({
    	effect : "fadeIn"
	});

	//BXSLIDER
	jQuery('.bxslider').bxSlider({
  		pagerCustom: '#bx-pager',
  		auto: true
	});

	//CHECKBOX
	jQuery('input[type=radio].radiobox, input[type=checkbox].cxbox').customRadioCheck();
	
});



